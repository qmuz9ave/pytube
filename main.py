from pytube import YouTube
import os
import sys
import ffmpeg


def progress_check(stream=None, chunk=None, remaining=None):
    percent = (100*(file_size-remaining))/file_size
    sys.stdout.write("\033[F")
    print("{:00.0f}% downloaded".format(percent))


def start():
    temp = "/home/rkp/Downloads/temp/"
    downloads = "/home/rkp/Downloads/"

    # input url
    url = input("ENTER THE URL: ")
    print(url)
    print('.........CHECKING YOUR URL............\n\n')

    try:
        data = YouTube(url, on_progress_callback=progress_check)
        files = data.streams.filter(file_extension='mp4')
        files_audioOnly = data.streams.filter(
            file_extension='mp4', only_audio=True).first()
    except:
        print("SORRY SOMETHING WENT WRONG \n\n")
        redo = start()

    print("**************AVAILABLE FORMAT****************")
    for i in range(len(files)):
        print("{} RESOLUTION=>{} AUDIO=>{}".format(
            i, files[i].resolution, files[i].is_progressive))
    print('\n\n')
    key = input('SELECT VIDEO TO DOWNLOAD: ')
    key = int(key)
    selected = files[key]
    title = selected.title
    global file_size
    file_size = selected.filesize
    # adaptive means video contains video only without sound and we have to download sound in seperate and merge
    if(selected.is_adaptive):
        # download has three arguments self,output_path,filename
        print('DOWNLOADING VIDEO FILE ..\n')
        video_path = selected.download(output_path=temp)
        print('DOWNLOADING AUDIO FILE ..\n')
        audio_path = files_audioOnly.download(
            output_path=temp, filename=title+'-AUDIO')
        audio_stream = ffmpeg.input(audio_path)
        video_stream = ffmpeg.input(video_path)
        output = downloads+title+'.mp4'
        ffmpeg.output(audio_stream, video_stream, output).run()
        print('YOUR FILE HAS BEEN SAVED TO \n {}'.format(downloads))

    else:
        # download has three arguments self,output_path,filename
        print('DOWNLOADING VIDEO FILE ..\n')
        video_path = selected.download(output_path=downloads)
        print('YOUR FILE HAS BEEN SAVED TO \n {}'.format(downloads))
    print("READY TO DOWNLOAD ANOTHER VIDEO.\n\n")
    again = start()


file_size = 0
begin = start()
